
# coding: utf-8

# In[1]:


get_ipython().magic('matplotlib inline')
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import math
from functools import partial


# In[2]:


N = 4
alpha = 2 + 0.1 * N


# In[3]:


alpha


# Общий вид диф ура:
# <br><br>
# y'' = y + 6.8 + 2.4x(1-x), y(1) = e + 1/e -2, y'(0) = -2.4 .

# Аналитическое решение через wolfram:

# In[4]:


def analytical_solution(x):
    return 2.4*np.power(x, 2) - 2.4*x + np.exp(-x) + np.exp(x) - 2


# In[5]:


analytical_solution(1)


# In[6]:


# вспомогательная фнукция для инициализации массивов X и Y
def init(range_=(0, 1), N=10):
    x_points = np.append(np.linspace(range_[0], range_[1], N, endpoint=False), [range_[1]])
    y_points = np.zeros(x_points.shape[0]) # инициализируем нулями
    step = 1/N
    return x_points, y_points, step


# In[7]:


def analytical_solution_in_range(range_=(0, 1), N=10):
    x_points, y_points, step = init(range_, N)
    y_points = analytical_solution(x_points)
    return x_points, y_points


# In[8]:


def plot(methods, N):
    method_to_xs, method_to_ys = {}, {}
    methods['analytical'] = analytical_solution_in_range
    for name, method in methods.items():
        if name == 'analytical':
            xs, ys = method(N=2000)
        else:
            xs, ys = method(N=N)
        method_to_xs[name] = xs
        method_to_ys[name] = ys

    plt.figure(figsize=(12, 7))
    for name, ys in method_to_ys.items():
        if name != 'analytical':
            plt.plot(method_to_xs[name], ys, '--', label=name)
        else:
            plt.plot(method_to_xs[name], ys, label=name)
    plt.legend(loc='upper left')
    plt.show()


# ### 1. Метод разностной прогонки с погрешностью O(h) через метод Гаусса-Зейделя

# In[9]:


# решение через систему линейных уравнений, np.linalg.solve на основе метода Гаусса-Зейделя, погрешность начальных условий O(h)

def differential_sweep_method_o_h_error_linalg_solver(range_=(0,1), N=10):
    x_points, y_points, step = init(range_, N)
    matrix = np.zeros(shape=(N, N))
    # считаем правые части СЛУ:
    x_vector = 6.8 + (2.4*x_points)*(1-x_points)
    x_vector[0] = -2.4 # нашли правую часть на основании начального условия на левом конце
    x_vector[-1] = math.e + 1/math.e - 2 # нашли правую часть на основании начального условия на правом конце
    
    # расчет матрицы коэффициентов
    matrix = np.zeros((N+1, N+1))
    # инициализируем 0-ое уравнение и n+1-ое уравнение вручную
    matrix[0, 0] = -1/step
    matrix[0, 1] = 1/step
    matrix[-1, -1] = 1
    
    prev = 1/(step**2)
    cur = -(2/(step**2) + 1)
    next_ = 1/(step**2)
    
    start = 1
    for i in range(1, N):
        matrix[i, start-1] = prev
        matrix[i, start] = cur
        matrix[i, start+1] = next_
        start += 1
    
    y_points = np.linalg.solve(matrix, x_vector)
    return x_points, y_points


# ### 2. Метод разностной прогонки с погрешностью O(h^2) через метод Гаусса-Зейделя

# In[10]:


# решение через систему линейных уравнений, np.linalg.solve на основе метода Гаусса-Зейделя, погрешность начальных условий O(h^2)

def differential_sweep_method_o_h_2_error_linalg_solver(range_=(0,1), N=10):
    x_points, y_points, step = init(range_, N)
    matrix = np.zeros(shape=(N, N))
    # считаем правые части СЛУ:
    x_vector = 6.8 + (2.4*x_points)*(1-x_points)
    x_vector[0] = 4.8/step - 6.8 # нашли правую часть на основании начального условия на левом конце
    x_vector[-1] = math.e + 1/math.e - 2 # нашли правую часть на основании начального условия на правом конце
    
    # расчет матрицы коэффициентов
    matrix = np.zeros((N+1, N+1))
    # инициализируем 0-ое уравнение и n+1-ое уравнение вручную
    matrix[0, 0] = 2/(step**2) + 1
    matrix[0, 1] = -2/(step**2)
    matrix[-1, -1] = 1
    
    prev = 1/(step**2)
    cur = -(2/(step**2) + 1)
    next_ = 1/(step**2)
    
    start = 1
    for i in range(1, N):
        matrix[i, start-1] = prev
        matrix[i, start] = cur
        matrix[i, start+1] = next_
        start += 1
    
    y_points = np.linalg.solve(matrix, x_vector)
    return x_points, y_points


# ### 3. Метод разностной прогонки с погрешностью O(h) через свойства трехдиагональной матрицы

# In[11]:


# решение через метод разностной прогонки на основе рекуррентных формул, погрешность начальных условий O(h)

def differential_sweep_method_o_h_error_formulas(range_=(0,1), N=10):
    x_points, y_points, step = init(range_, N)
    
    x_vector = 6.8 + (2.4*x_points)*(1-x_points)
    x_vector[0] = -2.4
    x_vector[-1] = math.e + 1/math.e - 2 
    
    lambdas = np.zeros(x_points.shape[0])
    mus = np.zeros(x_points.shape[0])
    
    # init lambda and mus
    lambdas[-1] = 0
    mus[-1] = math.e + 1/math.e - 2
    
    lambdas[0] = -(1/step)/(-1/step)
    mus[0] = x_vector[0]/(-1/step)
    
    A_i = 1/(step**2)
    B_i = -(2/(step**2) + 1)
    C_i = 1/(step**2)
    
    for i in range(1, N):
        lambdas[i] = -C_i/(A_i*lambdas[i-1] + B_i)
        mus[i] = (x_vector[i] - A_i*mus[i-1])/(A_i*lambdas[i-1] + B_i)
    
    y_points[-1] = math.e + 1/math.e - 2
    
    for i in reversed(range(N)):
        y_points[i] = lambdas[i]*y_points[i+1] + mus[i]
        
    return x_points, y_points


# ### 4. Метод разностной прогонки с погрешностью O(h^2) через свойства трехдиагональной матрицы

# In[12]:


# решение через метод разностной прогонки на основе рекуррентных формул, погрешность начальных условий O(h^2)

def differential_sweep_method_o_h_2_error_formulas(range_=(0,1), N=10):
    x_points, y_points, step = init(range_, N)
    
    x_vector = 6.8 + (2.4*x_points)*(1-x_points)
    x_vector[0] = 4.8/step - 6.8 # нашли правую часть на основании начального условия на левом конце
    x_vector[-1] = math.e + 1/math.e - 2
    
    lambdas = np.zeros(x_points.shape[0])
    mus = np.zeros(x_points.shape[0])
    
    lambdas[-1] = 0
    mus[-1] = math.e + 1/math.e - 2
    
    lambdas[0] = -(-2/(step**2))/(2/(step**2) + 1)
    mus[0] = x_vector[0]/(2/(step**2) + 1)
    
    A_i = 1/(step**2)
    B_i = -(2/(step**2) + 1)
    C_i = 1/(step**2)
    
    for i in range(1, N):
        lambdas[i] = -C_i/(A_i*lambdas[i-1] + B_i)
        mus[i] = (x_vector[i] - A_i*mus[i-1])/(A_i*lambdas[i-1] + B_i)
    
    y_points[-1] = math.e + 1/math.e - 2
    
    for i in reversed(range(N)):
        y_points[i] = lambdas[i]*y_points[i+1] + mus[i]
        
    return x_points, y_points


# In[13]:


# z_0, y_0 -инициализация краевых условий

def init_shooting(range_=(0,1),N=10, z_0=-2.4, y_0=0):
    x_points = np.append(np.linspace(range_[0], range_[1], N, endpoint=False), [range_[1]])
    y_points = np.zeros(x_points.shape[0])
    z_points = np.zeros(x_points.shape[0])
    step = 1/N
    
    y_points[0] = y_0
    z_points[0] = z_0
    
    return x_points, y_points, z_points, step

def z_derivative(x, y, z):
    return y + 6.8 + 2.4*x*(1-x)

def y_derivative(x, y, z):
    return z


# In[14]:


right_bound = math.e + 1/math.e - 2


# In[15]:


def F_value(y):
    return y - right_bound


# In[16]:


def euler_explicit_method(range_=(0, 1), N=10, z_0=-2.4, y_0=0, y_derivative=y_derivative, z_derivative=z_derivative):
    x_points, y_points, z_points, step = init_shooting(range_, N, z_0, y_0)
    
    for i in range(1, x_points.shape[0]):
        y_points[i] = y_points[i-1] + step * y_derivative(x_points[i-1], y_points[i-1], z_points[i-1])
        z_points[i] = z_points[i-1] + step * z_derivative(x_points[i-1], y_points[i-1], z_points[i-1])
        
    return x_points, y_points, z_points


# In[17]:


def euler_with_recounting(range_=(0, 1), N=10, z_0=-2.4, y_0=0, y_derivative=y_derivative, z_derivative=z_derivative):
    x_points, y_points, z_points, step = init_shooting(range_, N, z_0, y_0)
    
    for i in range(1, x_points.shape[0]):
        f1_prev = y_derivative(x_points[i-1], y_points[i-1], z_points[i-1])
        f2_prev = z_derivative(x_points[i-1], y_points[i-1], z_points[i-1])

        y_tilde = y_points[i-1] + step*f1_prev
        z_tilde = z_points[i-1] + step*f2_prev

        y_points[i] = y_points[i-1] + (step/2)*(f1_prev + y_derivative(x_points[i], y_tilde, z_tilde))
        z_points[i] = z_points[i-1] + (step/2)*(f2_prev + z_derivative(x_points[i], y_tilde, z_tilde))
        
    return x_points, y_points, z_points


# In[18]:


def runge_kutta_4th_order(range_=(0, 1), N=10, z_0=-2.4, y_0=0, y_derivative=y_derivative, z_derivative=z_derivative):
    x_points, y_points, z_points, step = init_shooting(range_, N, z_0, y_0)
    
    for i in range(1, x_points.shape[0]):
        k1_y = step * y_derivative(x_points[i-1], y_points[i-1], z_points[i-1])
        k1_z = step * z_derivative(x_points[i-1], y_points[i-1], z_points[i-1])
        
        k2_y = step * y_derivative(x_points[i-1] + step/2, y_points[i-1] + k1_y/2, z_points[i-1] + k1_z/2)
        k2_z = step * z_derivative(x_points[i-1] + step/2, y_points[i-1] + k1_y/2, z_points[i-1] + k1_z/2)
        
        k3_y = step * y_derivative(x_points[i-1] + step/2, y_points[i-1] + k2_y/2, z_points[i-1] + k2_z/2)
        k3_z = step * z_derivative(x_points[i-1] + step/2, y_points[i-1] + k2_y/2, z_points[i-1] + k2_z/2)
        
        k4_y = step * y_derivative(x_points[i], y_points[i-1] + k3_y, z_points[i-1] + k3_z)
        k4_z = step * z_derivative(x_points[i], y_points[i-1] + k3_y, z_points[i-1] + k3_z)
        
        y_points[i] = y_points[i-1] + (1/6) * (k1_y + 2*k2_y + 2*k3_y + k4_y)
        z_points[i] = z_points[i-1] + (1/6) * (k1_z + 2*k2_z + 2*k3_z + k4_z)
        
    return x_points, y_points, z_points


# In[19]:


def shooting_method_with_secant_solver(method, range_=(0, 1), N=10, z_0=-2.4, y_0=0, eps=5*10**(-6), max_iter=100):
    n = 0
    initial_approximation = y_0
    initial_value = None
    
    while n < max_iter:
        x_points, y_points, _ = method(range_=range_, N=N, z_0=z_0, y_0=y_0)
        if n == 0:
            initial_value = y_points[-1]
            y_0 = 0.01
            
        if n>=1:
            new_approximation = y_0 - ((y_points[-1] - right_bound)*(y_0 - initial_approximation))/(y_points[-1] - initial_value)

            if abs(y_points[-1] - right_bound) <= eps:
                return x_points, y_points
            y_0 = new_approximation
        n+=1
    return x_points, y_points


# In[20]:


# функции для системы, к которой сводится внутренняя задача Коши для метода Ньютона
# здесь внутренняя задача Коши имеет вид: y''=y, y(0)=1, y'(0)=0
def newton_y_derivative(x, y, z):
    return z

def newton_z_derivative(x, y, z):
    return y

def shooting_method_with_newton_solver(method, range_=(0, 1), N=10, z_0=-2.4, y_0=0, eps=5*10**(-6), max_iter=1000):
    n = 0
    while n < max_iter:
        x_points, y_points, _ = method(range_=range_, N=N, z_0=z_0, y_0=y_0)
        
        if abs(F_value(y_points[-1])) <= eps:
            return x_points, y_points
        
        # решаем внутренюю задачу Коши, чтобы посчитать новое приближение
        x_points_inner, y_points_inner, _ = method(range_=range_, N=N, z_0=0, y_0=1, y_derivative=newton_y_derivative,
                                                   z_derivative=newton_z_derivative)
        # новое приближение
        y_0 = y_0 - F_value(y_points[-1])/y_points_inner[-1]
        n += 1
    return x_points, y_points


# In[21]:


newton_explicit_euler_method = partial(shooting_method_with_newton_solver, euler_explicit_method)
newton_euler_with_recounting_method = partial(shooting_method_with_newton_solver, euler_with_recounting)
newton_runge_kutta_4th_order_method = partial(shooting_method_with_newton_solver, runge_kutta_4th_order)


# In[30]:


methods = {
    'Метод Cтрельбы, явный метод Эйлера' : newton_explicit_euler_method,
    'Метод Стрельбы, метод Эйлера с пересчетом' : newton_euler_with_recounting_method,
    'Метод Стрельбы, метод Рунге-Кутта 4-го порядка' : newton_runge_kutta_4th_order_method,
    'Метод разностной прогонки с погрешностью O(h)' : differential_sweep_method_o_h_error_formulas,
    'Метод разностной прогонки с погрешностью O(h^2)' : differential_sweep_method_o_h_2_error_formulas
}


# In[34]:


plot(methods, N=100)

