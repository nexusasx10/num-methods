import math
import matplotlib.pyplot as plt
from matplotlib import mlab


COLORS = []
for red in ['00', 'FF']:
    for green in ['00', 'FF']:
        for blue in ['00', 'FF']:
            COLORS.append('#' + red + green + blue + 'FF')


def method(name):
    def _method(func):
        func.name = name
        return func
    return _method


def solve_real(y):
    def _solve_real(xs, f, y0, h):
        ys = [y(x) for x in xs]
        return ys
    return _solve_real


def solve_tailor_3_sys(xs, f, dfx, dfy, dfx2, dfy2, y0, z0, h):
    ys = [None for _ in xs]
    zs = [None for _ in xs]
    ys[0] = y0
    zs[0] = z0

    g = lambda z: z

    for i in range(1, len(xs)):
        ys[i] = ys[i - 1] + \
            h * g(zs[i - 1]) + \
            (h**2 / 2) * f(xs[i - 1], ys[i - 1]) + \
            (h**3 / 6) * (dfx(xs[i - 1], ys[i - 1]))

        zs[i] = zs[i - 1] + \
            h * f(xs[i - 1], ys[i - 1]) + \
            (h**2 / 2) * (dfx(xs[i - 1], ys[i - 1]) + dfy(xs[i - 1], ys[i - 1]) * f(xs[i - 1], ys[i - 1])) + \
            (h**3 / 6) * (dfx2(xs[i - 1], ys[i - 1]) + f(xs[i - 1], ys[i - 1]) ** 2 * dfy2(xs[i - 1], ys[i - 1]) + (dfx(xs[i - 1], ys[i - 1]) + dfy(xs[i - 1], ys[i - 1]) * f(xs[i - 1], ys[i - 1])) * dfy(xs[i - 1], ys[i - 1]))

    return ys, zs


@method("Стрельба")
def shooting(xs, f, dfx, dfy, dfx2, dfy2, y0, h, s):
    mu = 0
    v = lambda x: 0.5 * math.e ** (-x) + 0.5 * math.e ** x
    dv = lambda x: -0.5 * math.e ** (-x) + 0.5 * math.e ** x

    def g(mu):
        ys, dys = solve_tailor_3_sys(xs, f, dfx, dfy, dfx2, dfy2, y0, mu, h)
        return ys[-1] + dys[-1] - s

    dg = lambda mu: v(5) + dv(5)

    i = 0
    while True:
        new_mu = newton(mu, g, dg)
        if abs(new_mu - mu) < 0.1E-10:
            break
        mu = new_mu
        print(f'{i}: {mu}')
        i += 1

    ys, dys = solve_tailor_3_sys(xs, f, dfx, dfy, dfx2, dfy2, y0, mu, h)
    return ys


def newton(mu, g, dg):
    return mu - (g(mu) / dg(mu))


@method("Прогонка")
def shuttle(xs, p, q, y0, s, r, h):
    xs = list(xs)
    n = len(xs)

    # Коэфициенты при yn-1 в системе
    As = [1 for _ in xs]
    As[0] = 0
    As[n-1] = 2

    # Коэфициенты при yn в системе
    Cs = [-(2 + p(x) * h * h) for x in xs]
    Cs[0] = 1
    Cs[n-1] = - 2 * h * r - 2 - p(xs[n-1]) * h * h

    # Коэфициенты при yn+1 в системе
    Bs = [1 for _ in xs]
    Bs[0] = 0
    Bs[n-1] = 0

    # Правая часть системы
    Fs = [q(x) * h * h for x in xs]
    Fs[0] = y0
    Fs[n-1] = - 2 * h * s + q(xs[n-1]) * h * h

    alfas = [None for _ in xs]
    betas = [None for _ in xs]

    alfas[1] = -Bs[0] / Cs[0]
    betas[1] = Fs[0] / Fs[0]
    for i in range(2, n):
        alfas[i] = -Bs[i-1] / (As[i-1] * alfas[i-1] + Cs[i-1])
        betas[i] = (Fs[i-1] - As[i-1] * betas[i-1]) / (As[i-1] * alfas[i-1] + Cs[i-1])

    ys = [None for _ in xs]
    ys[n-1] = (Fs[n-1] - As[n-1] * betas[n-1]) / (Cs[n-1] + As[n-1] * alfas[n-1])

    for i in range(n-2, -1, -1):
        ys[i] = alfas[i+1] * ys[i+1] + betas[i+1]

    return ys


def main():
    # y'' = p(x)y + q(x)
    # y(a) = y0
    # y'(b) + r*y(b) = s

    p = lambda x: 1
    q = lambda x: 5 + 2.5 * x * x * (x - 1)
    f = lambda x, y: y + q(x)

    dfx = lambda x, y: 7.5 * x * x - 5 * x
    dfy = lambda x, y: 1
    dfx2 = lambda x, y: 15 * x - 5
    dfy2 = lambda x, y: 0

    a = 0
    b = 5
    y0 = 3
    s = 2 * math.e ** 5 - 502.5
    r = 1

    solution = lambda x: 2 * math.e ** (-x) + math.e ** x + x * (-2.5 * x * x + 2.5 * x - 15)

    hs = [0.5, 0.05]

    draw1(a, b, f, y0, hs, solution, p, q, s, r)
    draw2(a, b, f, dfx, dfy, dfx2, dfy2, y0, hs, solution, s)


def draw2(a, b, f, dfx, dfy, dfx2, dfy2, y0, hs, y, s):
    fig = plt.figure()
    legend = []

    h = 0.0001
    xs = mlab.frange(a, b, h)
    legend.append('Точное решение')
    ys = solve_real(y)(xs, f, y0, h)
    ax = fig.add_subplot(111)
    ax.plot(xs, ys, color=COLORS[0], lw=1)

    i = 1
    for h in hs:
        xs = mlab.frange(a, b, h)
        ys = shooting(xs, f, dfx, dfy, dfx2, dfy2, y0, h, s)
        legend.append(f'{shooting.name} h={h}')
        ax = fig.add_subplot(111)
        ax.plot(xs, ys, color=COLORS[i], lw=0.5)
        i += 1

    plt.ylabel('Y(X)')
    plt.xlabel('X')
    fig.legend(legend)

    plt.show()


def draw1(a, b, f, y0, hs, y, p, q, s, r):
    fig = plt.figure()
    legend = []

    h = 0.0001
    xs = mlab.frange(a, b, h)
    legend.append('Точное решение')
    ys = solve_real(y)(xs, f, y0, h)
    ax = fig.add_subplot(111)
    ax.plot(xs, ys, color=COLORS[0], lw=1)

    i = 1
    for h in hs:
        xs = mlab.frange(a, b, h)
        ys = shuttle(xs, p, q, y0, s, r, h)
        legend.append(f'{shuttle.name} h={h}')
        ax = fig.add_subplot(111)
        ax.plot(xs, ys, color=COLORS[i], lw=0.5)
        i += 1

    plt.ylabel('Y(X)')
    plt.xlabel('X')
    fig.legend(legend)

    plt.show()


if __name__ == '__main__':
    main()
