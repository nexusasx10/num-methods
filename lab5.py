import math
import matplotlib.pyplot as plt
from matplotlib import mlab


COLORS = []
for r in ['00', 'FF']:
    for g in ['00', 'FF']:
        for b in ['00', 'FF']:
            COLORS.append('#' + r + g + b + 'FF')


def method(name):
    def _method(func):
        func.name = name
        return func
    return _method


def solve_real(y):
    def _solve_real(xs, f, y0, h):
        ys = [y(x) for x in xs]
        return ys
    return _solve_real


def simple_iter(x, f):
    err = 100
    i = 0
    while err > 0.1E-10:
        xnew = f(x)
        err = abs(x - xnew)
        x = xnew
        i += 1
        if i > 1000:
            print("Warning: simple iteration method has failed. Inaccurate result will be returned.")
            return x
    return x


@method('Явный Рунге-Кутты')
def solve_runge_kutta_explicit(xs, f, y0, h):
    ys = [None for _ in xs]
    ys[0] = y0

    k1 = lambda x, y: h * f(x, y)
    k2 = lambda x, y: h * f(x + h / 2, y + k1(x, y) / 2)
    k3 = lambda x, y: h * f(x + h / 2, y + k2(x, y) / 2)
    k4 = lambda x, y: h * f(x + h, y + k3(x, y))

    for i in range(1, len(xs)):
        ys[i] = ys[i-1] + (1/6) * k1(xs[i-1], ys[i-1]) + (2/6) * k2(xs[i-1], ys[i-1]) + (2/6) * k3(xs[i-1], ys[i-1]) + (1/6) * k4(xs[i-1], ys[i-1])

    return ys


@method('Явный Адамса')
def solve_adams_explicit(xs, f, y0, h):
    ys = [None for _ in xs]
    ys[0] = y0

    yb = solve_runge_kutta_explicit(xs, f, y0, h)
    ys[1] = yb[1]

    for i in range(2, len(xs)):
        ys[i] = ys[i-1] + (h/2) * (3 * f(xs[i-1], ys[i-1]) - f(xs[i-2], ys[i-2]))

    return ys


@method('Неявный Адамса')
def solve_adams_implicit(xs, f, y0, h):
    ys = [None for _ in xs]
    ys[0] = y0

    yb = solve_runge_kutta_explicit(xs, f, y0, h)
    ys[1] = yb[1]

    for i in range(2, len(xs)):
        g = lambda yip1: ys[i-1] + (h/12) * (5 * f(xs[i], yip1) + 8 * f(xs[i-1], ys[i-1]) - f(xs[i-2], ys[i-2]))
        ys[i] = simple_iter(ys[i-1], g)

    return ys


def main():
    f = lambda x, y: 50 * y * (x - 0.6) * (x - 0.85)
    y = lambda x: 0.1 * math.e ** (50 * x * (x ** 2 / 3 - 1.45 / 2 * x + 0.51))
    y0 = 0.1
    a = 0
    b = 1
    hs = [0.05, 0.01]
    draw(
        (solve_runge_kutta_explicit, solve_adams_explicit, solve_adams_implicit),
        a, b, f, y0, hs, y
    )


def draw(methods, a, b, f, y0, hs, y):
    for method in methods:
        fig = plt.figure()
        legend = []

        h = 0.0001
        xs = mlab.frange(a, b, h)
        legend.append('Точное решение')
        ys = solve_real(y)(xs, f, y0, h)
        ax = fig.add_subplot(111)
        ax.plot(xs, ys, color=COLORS[0], lw=1)

        i = 1
        for h in hs:
            xs = mlab.frange(a, b, h)
            ys = method(xs, f, y0, h)
            legend.append(f'{method.name} h={h}')
            ax = fig.add_subplot(111)
            ax.plot(xs, ys, color=COLORS[i], lw=0.5)
            i += 1

        plt.ylabel('Y(X)')
        plt.xlabel('X')
        fig.legend(legend)

        plt.show()


if __name__ == '__main__':
    main()
