import math
import pylab
import matplotlib.pyplot as plt
from matplotlib import mlab

COLORS = ["#00FF00AA", "#0000FFAA"]


def y(x):
    return 0.1 * (math.e ** (10 * x ** 3 - 13.5 * x ** 2 + 4.2 * x))


def paint_analysts_sol(xlist):
    ylist = [y(x) for x in xlist]

    pylab.plot(xlist, ylist, color='#FF0000')
    pylab.ylabel('Y(X)')
    pylab.xlabel('X')
    pylab.title('График 1')
    pylab.show()


def f(x, y):
    return 30 * y * (x - 0.2) * (x - 0.7)


def adams_b(x_list, y_list, i, h):
    if i < 2:
        return teylora_met(x_list, y_list, i, h)
    else:
        fn = f(x_list[i], y_list[i])
        fn1 = f(x_list[i - 1], y_list[i - 1])
        fn2 = f(x_list[i - 2], y_list[i - 2])
        return y_list[i] + h / 12 * (23 * fn - 16 * fn1 + 5 * fn2)


def teylora_met(x_list, y_list, i, h):
    _x = x_list[i]
    _y = y_list[i]

    def df(x, y):
        return 0.12 * (
                    7500 * x ** 4 - 13500 * x ** 3 + 8175 * x ** 2 - 1390 * x - 78) * y

    def ddf(x, y):
        return 60 * y + (60 * x - 27) * f(x, y) + (60 * x - 27) + (
                    30 * x ** 2 - 27 * x + 4.2) * df(x, y)

    return _y + h * f(_x, _y) + (h ** 2) / 2 * df(_x, _y) + \
           (h ** 3) / 6 * ddf(_x, _y)


def simpson(x_list, y_list, i, h):
    if i < 3:
        return teylora_met(x_list, y_list, i, h)
    else:
        y_next = y(x_list[i])
        fn = f(x_list[i + 1], y_next)
        fn1 = f(x_list[i], y_list[i])
        fn2 = f(x_list[i - 1], y_list[i - 1])
        return y_list[i - 1] + h / 3 * (fn2 + 4 * fn1 + fn)


def runge_kutta(x_list, y_list, i, h):
    pass


def paint(method, h_list, a, b, name):
    c = 0
    fig = plt.figure()
    legend = ['Точное решение']
    sx = fig.add_subplot(111)
    sx.plot(*get_source_value(), color='#FF0000AA', lw=0.5)

    for h in h_list:
        x_list = mlab.frange(a, b, h)
        y_list = [0.1]
        legend.append('h=' + str(h))
        for i in range(1, len(x_list), 1):
            y = method(x_list, y_list, i - 1, h)
            y_list.append(y)

        ax = fig.add_subplot(111)
        ax.plot(x_list, y_list, color=COLORS[c], lw=0.5)
        c += 1

    plt.ylabel('Y(X)')
    plt.xlabel('X')
    fig.legend(legend)
    pylab.title(name)

    plt.show()


def get_source_value():
    h = 0.000001
    x_list = mlab.frange(a, b, h)
    y_list = [y(x) for x in x_list]
    return (x_list, y_list)


if __name__ == '__main__':
    a = 0
    b = 1

    paint(simpson, [0.1, 0.05], a, b, 'График - Симпсон')
    paint(teylora_met, [0.1, 0.05], a, b, 'График - Тейлора')
    paint(adams_b, [0.1, 0.05], a, b, 'График - Адамса')
