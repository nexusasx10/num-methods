import math


def solve_simpson(a, b, f, df, h):
    s = 0
    ai = a
    bi = ai + h
    i = 0
    while bi <= b + 0.0000000001:
        s += f(ai) + 4 * f(ai + h / 2) + f(bi)
        ai += h
        bi += h
        i += 1
    print(f'iter={i}')
    return (h / 6) * s


def solve_euler_maclaurin(a, b, f, df, h):
    s = 0
    ai = a
    bi = ai + h
    i = 0
    while bi <= b + 0.0000000001:
        s += f(ai) + f(bi)
        ai += h
        bi += h
        i += 1
    print(f'iter={i}')
    return (h / 2) * s + (h ** 2 / 12) * (df(a) - df(bi - h))


def solve_gauss(f):
    return f(-(3 ** 0.5) / 3) + f((3 ** 0.5) / 3)


def error(method, a, b, f, df, h, p):
    return (2 ** p * (method(a, b, f, df, h/2) - method(a, b, f, df, h))) / (2 ** p - 1)


def main():
    a = 1.9
    b = 4.3
    f = lambda x: math.e ** (x + math.sin(x))
    df = lambda x: (1 + math.cos(x)) * math.e ** (x + math.sin(x))

    f2 = lambda t: 1.2 * math.e ** (1.2 * t + 3.1 + math.sin(1.2 * t + 3.1))

    hs = [0.1, 0.05, 0.025]

    print('Simpson')
    for h in hs:
        print(solve_simpson(a, b, f, df, h))
        print(error(solve_simpson, a, b, f, df, h, 4))
        print()

    print('Euler-Maclaurin')
    for h in hs:
        print(solve_euler_maclaurin(a, b, f, df, h))
        print(error(solve_euler_maclaurin, a, b, f, df, h, 4))
        print()

    print('Gauss')
    print(solve_gauss(f2))


if __name__ == '__main__':
    main()
